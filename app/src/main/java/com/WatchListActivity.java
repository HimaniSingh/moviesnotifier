package com;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;

import com.upcoming.hollywoodmovies.R;

import java.util.ArrayList;

public class WatchListActivity extends AppCompatActivity  implements WatchlistAdpter.Callback{

    RecyclerView recyler;
    WatchlistAdpter watchlistAdpter;
    DatabaseHelper databaseHelper;
    private ArrayList<DataModel> list;
    ImageView back,dimage;
    TextView resulationtext;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_list);
        recyler = findViewById(R.id.recyler);
        toolbar = findViewById(R.id.toolbar);
        dimage = findViewById(R.id.dimage);
        back = toolbar.findViewById(R.id.back);
        resulationtext=toolbar.findViewById(R.id.resulationtext);
        // resulationtext.setVisibility(View.GONE);
        resulationtext.setText("Movies Watchlist");
        watchlistAdpter = new WatchlistAdpter( this,list);
        recyler.setHasFixedSize(true);
        recyler.setLayoutManager(new LinearLayoutManager(this));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        databaseHelper = new DatabaseHelper(this);
        list = databaseHelper.listtime();

        if (list.size() > 0) {
            recyler.setVisibility(View.VISIBLE);
            watchlistAdpter = new WatchlistAdpter(this, list);
            recyler.setAdapter(watchlistAdpter);
        } else {

            recyler.setVisibility(View.GONE);
            Toast.makeText(this, "There is no data to Watch", Toast.LENGTH_LONG).show();
        }




    }

    @Override
    public void onRowClicked(String id, boolean b) {
        dimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ArrayList<DataModel> list = watchlistAdpter.getNotificationList();
//                int s=list.size();
//                Log.i("hhh", String.valueOf(s));
//                for (int i=list.size()-1;i>=0;i--){
//
//                        databaseHelper.deletedata(Integer.parseInt(list.get(i).getId()));
//                        list.remove(i);
//                    }
//
//
//
//                //...........................
//                watchlistAdpter.setInfo(true);
//                watchlistAdpter.setList(list);
//                watchlistAdpter.notifyDataSetChanged();
//                // Toast.makeText(this,"DELETED SUCCESSFULLY",Toast.LENGTH_SHORT).show();
//                Toast.makeText(WatchListActivity.this, "DELETED SUCCESSFULLY", Toast.LENGTH_SHORT).show();


            }
        });





    }
}