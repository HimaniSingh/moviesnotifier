package com;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.upcoming.hollywoodmovies.R;

import java.util.ArrayList;

public class WatchlistAdpter extends RecyclerView.Adapter<WatchlistAdpter.ViewHolder> {

    private ArrayList<DataModel> list;
    Context context;
    Drawable icon;
    private Boolean isInfo = true;
    SharedPreferences sharedPreferences;
    SQLiteDatabase db;
    DatabaseHelper mDbHelper;
    int id=0;


    public void setInfo(Boolean info) {
        isInfo = info;
    }

    //    public void setList(ArrayList<Notifiylist> list) {
//        this.notify = list;
//    }
//    public void setList(ArrayList<DataModel> second) {
//        this.list = second;
//    }
//
    public WatchlistAdpter(Context context, ArrayList<DataModel> list) {
        this.list = list;
        this.context = context;
    }

    public ArrayList<DataModel> getNotificationList() {
        return list;
    }

    public void setList(ArrayList<DataModel> list1) {
        this.list = list1;
    }
//
//    public ArrayList<DataModel> getList() {
//        return list;
//    }

    @Override
    public WatchlistAdpter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.itemslayout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final WatchlistAdpter.ViewHolder holder, final int position) {
        final DataModel datalist = list.get(position);
        holder.title.setText(datalist.getTitle());
        holder.date.setText(datalist.getDate());
        holder.days.setText(datalist.getDay());



//        mDbHelper = new DatabaseHelper(context);
//
//        holder.delect.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                db.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper.C_ID + "=" + id, null);
//
//
//                Toast.makeText(context, "DELETED SUCCESSFULLY", Toast.LENGTH_SHORT).show();
//
//            }
//        });

//        holder.rvv.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                ((Callback) context).onRowClicked(datalist.getId(), true);
//               // int p= Integer.parseInt(datalist.getId());
//                return true;
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, watch_btn, time, date,days;
        ImageView imageInListView;
        RelativeLayout rvv;
        CheckBox check;
        View view;
        CardView lifebeautifulcard;
        ImageView delect;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            watch_btn = itemView.findViewById(R.id.watch_btn);
            // text = itemView.findViewById(R.id.text);
            time = itemView.findViewById(R.id.time);
            imageInListView = itemView.findViewById(R.id.app_icon);
            rvv = itemView.findViewById(R.id.rvv);
            check = itemView.findViewById(R.id.check);
            date = itemView.findViewById(R.id.date);
            days = itemView.findViewById(R.id.days);
            delect = itemView.findViewById(R.id.delect);
            lifebeautifulcard = itemView.findViewById(R.id.lifebeautifulcard);
            view = itemView;
        }
    }

    public interface Callback {
        void onRowClicked(String id, boolean b);
    }
    public void deleteNote(int note){
        db = mDbHelper.getWritableDatabase();
        db.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper.C_ID + "=" + id, null);
    }
}
