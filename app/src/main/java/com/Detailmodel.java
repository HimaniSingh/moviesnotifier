package com;

public class Detailmodel {
    private String description;
    private int imgId;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Detailmodel(int imgId, String description, String title) {
        this.description = description;
        this.imgId = imgId;
        this.title=title;
    }


    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public int getImgId() {
        return imgId;
    }
    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
}
