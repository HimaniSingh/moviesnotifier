package com;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.upcoming.hollywoodmovies.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class DetailAdpter extends RecyclerView.Adapter<DetailAdpter.ViewHolder> {
    private Detailmodel[] listdata;
    Context context;
    ArrayList<String> name;
    String t;
    ArrayList<String> personNames;
    ArrayList<String> emailIds;
    ArrayList<String> mobileNumbers;
    ArrayList<String> image;
    ArrayList<String> genre ;
    ArrayList<String> director;
    ArrayList<String> country ;
    ArrayList<String> laungae ;
    ArrayList<String> runtime ;
    ArrayList<String> overview ;
    ArrayList<String> video ;
    ArrayList<String> cast ;

    // RecyclerView recyclerView;
//    public DetailAdpter(Detailmodel[] listdata,Context context) {
//        this.listdata = listdata;
//        this.context=context;
//    }
  public DetailAdpter(Context context, ArrayList<String> personNames, ArrayList<String> emailIds, ArrayList<String> mobileNumbers,ArrayList<String> image,ArrayList<String> genre,ArrayList<String> director,ArrayList<String> laungae
          ,   ArrayList<String> runtime, ArrayList<String> country, ArrayList<String> overview,ArrayList<String> video,ArrayList<String> cast) {
      this.context = context;
      this.personNames = personNames;
      this.emailIds = emailIds;
      this.mobileNumbers = mobileNumbers;
      this.image = image;
      this.genre=genre;
      this.director=director;
      this.laungae=laungae;
      this.runtime=runtime;
      this.country=country;
      this.overview=overview;
      this.video=video;
      this.cast=cast;
    }

    @Override
    public DetailAdpter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.detaillayout, parent, false);
        DetailAdpter.ViewHolder viewHolder = new DetailAdpter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DetailAdpter.ViewHolder holder, final int position) {
        holder.textView.setText(personNames.get(position));

        holder.tv.setText(emailIds.get(position));

        Glide.with(context)
                .load(image.get(position))
                .into(holder.imageView);



//        final Detailmodel myListData = listdata[position];
//        holder.textView.setText(listdata[position].getDescription());
      //  holder.tv.setText(emailIds.get(position));
   //    holder.imageView.setImageResource(listdata[position].getImgId());
//
//
////        final int drawable = listdata[position].getImgId();
////        Bitmap bitmap = ((BitmapDrawable) int).getBitmap();
////        ByteArrayOutputStream baos = new ByteArrayOutputStream();
////        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
////        final byte[] b = baos.toByteArray();
////
//
//
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               // Toast.makeText(view.getContext(), "click on item: " + myListData.getDescription(), Toast.LENGTH_LONG).show();

                Intent intent=new Intent(context,FullMoviesDetailsActivity.class);
                intent.putExtra("name",personNames.get(position));
                intent.putExtra("release",emailIds.get(position));
                intent.putExtra("image",image.get(position));
                intent.putExtra("runtime",runtime.get(position));
                intent.putExtra("genure",genre.get(position));
                intent.putExtra("languge",laungae.get(position));
                intent.putExtra("director",director.get(position));
                intent.putExtra("counry",country.get(position));
                intent.putExtra("overview",overview.get(position));
                intent.putExtra("video",video.get(position));
                intent.putExtra("cast",cast.get(position));
                context.startActivity(intent);




            }
        });
    }


    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView,tv;
        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
            this.textView = (TextView) itemView.findViewById(R.id.textView);
            this.tv = (TextView) itemView.findViewById(R.id.tv);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout);
        }
    }
}
