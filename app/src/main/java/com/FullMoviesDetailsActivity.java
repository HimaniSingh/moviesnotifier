package com;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.upcoming.hollywoodmovies.MainActivity;
import com.upcoming.hollywoodmovies.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FullMoviesDetailsActivity extends AppCompatActivity {

    TextView movietitle,releasedtv,director1,cuntry,lang,time1,gen,overviewtxt,castid;
    ImageView imageView,videoss;
    TextView cancle,ok;
    RadioButton radiotxt,radiotxt2,radiotxt3;
    RadioGroup radioGroup;


    String selectedSuperStar;


    int notificationid = 1;
    DatabaseHelper databaseHelper;

    String dayDifference;

    Date d;
    String des;
     String rel;


    public static final String NOTIFICATION_CHANNEL_ID = "10001" ;
    private final static String default_notification_channel_id = "default" ;
   // Button btnDate ;
    Calendar cal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_movies_details);
         des=getIntent().getStringExtra("name");
         rel=getIntent().getStringExtra("release");
        String direc=getIntent().getStringExtra("director");
        String counry=getIntent().getStringExtra("counry");
        String languge=getIntent().getStringExtra("languge");
        final String time=getIntent().getStringExtra("runtime");
        String genure=getIntent().getStringExtra("genure");
        String overview=getIntent().getStringExtra("overview");
        final String video=getIntent().getStringExtra("video");
        final String casttext=getIntent().getStringExtra("cast");
        movietitle=findViewById(R.id.movietitle);
        releasedtv=findViewById(R.id.releasedtv);
        castid=findViewById(R.id.castid);
        gen=findViewById(R.id.gen);
        time1=findViewById(R.id.time);
        lang=findViewById(R.id.lang);
        cuntry=findViewById(R.id.cuntry);
        overviewtxt=findViewById(R.id.overviewtxt);
        videoss=findViewById(R.id.videoss);
        director1=findViewById(R.id.director1);
        imageView=findViewById(R.id.image);
        movietitle.setText(des);
        releasedtv.setText(rel);
        director1.setText(direc);
        lang.setText(languge);
        cuntry.setText(counry);
        gen.setText(genure);
        time1.setText(time);
        overviewtxt.setText(overview);
        castid.setText(casttext);




        final String imag=getIntent().getStringExtra("image");
        Glide.with(this)
                .load(imag)
                .into(imageView);


        videoss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(video));
                startActivity(intent);

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);



      fab.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
             final Dialog settingsDialog = new Dialog(FullMoviesDetailsActivity.this);
              LayoutInflater layoutInflater1 = LayoutInflater.from(FullMoviesDetailsActivity.this);
              settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
              settingsDialog.setContentView(layoutInflater1.inflate(R.layout.remindercard, null));
              settingsDialog.show();
              cancle=settingsDialog.findViewById(R.id.cancle);
              ok=settingsDialog.findViewById(R.id.ok);
              radiotxt=settingsDialog.findViewById(R.id.radiotxt1);
              radiotxt2=settingsDialog.findViewById(R.id.radiotxt2);
              radiotxt3=settingsDialog.findViewById(R.id.radiotxt3);
              radioGroup=settingsDialog.findViewById(R.id.radiogroup);



//              radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//                  @Override
//                  public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                      switch(i) {
//                          case R.id.radiotxt1:
//                              releasedtv.setText(rel);
//                             // Log.i("release date", String.valueOf(releasedtv.getText()));
//                              break;
//                          case R.id.radiotxt3:
//                              movietitle.getText();
//                              releasedtv.getText();
//                              Toast.makeText(getApplicationContext(),movietitle.getText(),Toast.LENGTH_LONG).show();
//                              break;
//                      }
//                  }
//              });

              cancle.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                      finish();
                  }
              });
              ok.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {

                      if (radiotxt.isChecked()) {
                         // selectedSuperStar = radiotxt.getText().toString();
                          SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                          try {
                               d = sdf.parse(rel);
                              Log.i("kljkhj", String.valueOf(d));
                              cal=Calendar.getInstance();
                              cal.setTime(d);


                              String date = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.getDefault()).format(new Date());
                             // Log.i("hlkjhgf", String.valueOf(date));

                              Date currentdates=null;
                              SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
                              String temp = date;
                              try {
                                  currentdates = formatter.parse(temp);
                                  Log.e("formated date ", date + "");
                              } catch (ParseException e) {
                                  e.printStackTrace();
                              }

                              Date date1;
                              Date date2;

                              SimpleDateFormat dates = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");

                              //Setting dates
                              date1 = dates.parse(String.valueOf(date));
                              date2 = dates.parse(String.valueOf(d));

                              //Comparing dates
                              long difference = Math.abs(date1.getTime() - date2.getTime());
                              long differenceDates = difference / (24 * 60 * 60 * 1000);

                              //Convert long to String
                               dayDifference = Long.toString(differenceDates);

                              Log.e("HERE","HERE: " + dayDifference);
                          } catch (ParseException ex) {
                              Log.v("Exception", ex.getLocalizedMessage());
                          }

                          databaseHelper = new DatabaseHelper(FullMoviesDetailsActivity.this);
                          long checkinsertdata = databaseHelper.insertQuery(des,rel,dayDifference);
                          if (checkinsertdata != -1) {
                              notificationid=(int)checkinsertdata;
                              setAlarm();
                              //  Log.i("himani", String.valueOf(time));
                              Toast.makeText(FullMoviesDetailsActivity.this, "Reminder set successfully", Toast.LENGTH_SHORT).show();
                          } else {
                              // Toast.makeText(context, "New Entry no insertedt", Toast.LENGTH_SHORT).show();
                          }


                      } else if (radiotxt3.isChecked()) {
                         // selectedSuperStar = radiotxt3.getText().toString();


                          SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                          try {
                              d = sdf.parse(rel);
                             // Log.i("kljkhj", String.valueOf(d));
                              cal=Calendar.getInstance();
                              cal.setTime(d);


                              String date = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.getDefault()).format(new Date());
                             // Log.i("hlkjhgf", String.valueOf(date));

                              Date currentdates=null;
                              SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
                              String temp = date;
                              try {
                                  currentdates = formatter.parse(temp);
                                  Log.e("formated date ", date + "");
                              } catch (ParseException e) {
                                  e.printStackTrace();
                              }

                              Date date1;
                              Date date2;

                              SimpleDateFormat dates = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");

                              //Setting dates
                              date1 = dates.parse(String.valueOf(date));
                              date2 = dates.parse(String.valueOf(d));

                              //Comparing dates
                              long difference = Math.abs(date1.getTime() - date2.getTime());
                              long differenceDates = difference / (24 * 60 * 60 * 1000);

                              //Convert long to String
                              dayDifference = Long.toString(differenceDates);

                              Log.e("HERE","HERE: " + dayDifference);
                          } catch (ParseException ex) {
                              Log.v("Exception", ex.getLocalizedMessage());
                          }
                          databaseHelper = new DatabaseHelper(FullMoviesDetailsActivity.this);
                          long checkinsertdata = databaseHelper.insertQuery(des, rel, dayDifference );
                          if (checkinsertdata != -1) {
                              notificationid=(int)checkinsertdata;
                              //  Log.i("himani", String.valueOf(time));
                              Toast.makeText(FullMoviesDetailsActivity.this, "Reminder set successfully", Toast.LENGTH_SHORT).show();
                          } else {
                              // Toast.makeText(context, "New Entry no insertedt", Toast.LENGTH_SHORT).show();
                          }
                      }
                     // Toast.makeText(getApplicationContext(),selectedSuperStar, Toast.LENGTH_LONG).show();
                      // print the value of selected super star
                     // Toast.makeText(FullMoviesDetailsActivity.this, "set Reminder successfully", Toast.LENGTH_SHORT).show();
                      settingsDialog.dismiss();
                  }
              });
          }
      });
//        Bundle extras = getIntent().getExtras();
//        byte[] b = extras.getByteArray("image");
//        Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);
//        imageView.setImageBitmap(bmp);

    }



    private void setAlarm() {
        Intent notificationIntent = new Intent(getApplicationContext(), MyNotificationPublisher.class);
        notificationIntent.putExtra("notificationId", notificationid);
        notificationIntent.putExtra("todo",des);
        notificationIntent.putExtra("dates",d);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) getSystemService(getApplicationContext().ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP,cal.getTimeInMillis(), 7 * 24 * 60 * 60 * 1000, pendingIntent);

        //  adManager.showInterstitialAd(getApplicationContext());

    }
}