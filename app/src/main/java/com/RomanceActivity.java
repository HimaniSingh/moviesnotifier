package com;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.upcoming.hollywoodmovies.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class RomanceActivity extends AppCompatActivity {
RecyclerView romrecyl;


    ArrayList<String> personNames = new ArrayList<>();
    ArrayList<String> emailIds = new ArrayList<>();
    ArrayList<String> mobileNumbers = new ArrayList<>();
    ArrayList<String> image = new ArrayList<>();
    ArrayList<String> genre = new ArrayList<>();
    ArrayList<String> director = new ArrayList<>();
    ArrayList<String> country = new ArrayList<>();
    ArrayList<String> laungae = new ArrayList<>();
    ArrayList<String> runtime = new ArrayList<>();
    ArrayList<String> overview = new ArrayList<>();
    ArrayList<String> video = new ArrayList<>();
    ArrayList<String> cast = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_romance);
        romrecyl=findViewById(R.id.romrecyl);


        romrecyl.setHasFixedSize(true);
        romrecyl.setLayoutManager(new LinearLayoutManager(this));
        // recyl.setAdapter(adapter);




        try {
            // get JSONObject from JSON file
            JSONObject obj = new JSONObject(dataSONFromAsset());
            // fetch JSONArray named users
            JSONArray userArray = obj.getJSONArray("users");
            // implement for loop for getting users list data
            for (int i = 0; i < userArray.length(); i++) {
                // create a JSONObject for fetching single user data
                JSONObject userDetail = userArray.getJSONObject(i);
                // fetch email and name and store it in arraylist
                personNames.add(userDetail.getString("name"));
                emailIds.add(userDetail.getString("email"));
                image.add(userDetail.getString("image"));
                genre.add(userDetail.getString("Genre"));
                director.add(userDetail.getString("Director"));
                laungae.add(userDetail.getString("Language"));
                runtime.add(userDetail.getString("Runtime"));
                country.add(userDetail.getString("country"));
                overview.add(userDetail.getString("overview"));
                video.add(userDetail.getString("video"));
                cast.add(userDetail.getString("cast"));
                // create a object for getting contact data from JSONObject
                JSONObject contact = userDetail.getJSONObject("contact");
                // fetch mobile number and store it in arraylist
                mobileNumbers.add(contact.getString("mobile"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        DetailAdpter customAdapter = new DetailAdpter(this, personNames, emailIds, mobileNumbers,image,genre,director,laungae,runtime,country,overview,video,cast);
        romrecyl.setAdapter(customAdapter);
    }



    public String dataSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("romantic.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

}
}