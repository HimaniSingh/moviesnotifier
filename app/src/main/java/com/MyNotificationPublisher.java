package com;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.upcoming.hollywoodmovies.MainActivity;
import com.upcoming.hollywoodmovies.R;

import static com.FullMoviesDetailsActivity.NOTIFICATION_CHANNEL_ID;

public class MyNotificationPublisher extends BroadcastReceiver {
    String dates;
    String message;
    long t;

    int notificationid;
    public static final String CHANNELID = "channelID";
    @Override
    public void onReceive(Context context, Intent intent) {
     //   Toast.makeText(context, "recevier start", Toast.LENGTH_SHORT).show();


        notificationid=intent.getIntExtra("notificationId",0);
        message=intent.getStringExtra("todo");
        dates=intent.getStringExtra("dates");
        t=intent.getLongExtra("time",0);
        Log.i("himoo",notificationid+""+message+""+t+dates);

        Intent mainIntent = new Intent(context, WatchListActivity.class);
        mainIntent.putExtra("notify",notificationid);
        mainIntent.putExtra("names",message);
        mainIntent.putExtra("datesall",dates);
        mainIntent.putExtra("ttimes",t);
        PendingIntent pendingIntent=PendingIntent.getActivity(context,0,mainIntent,PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager manager=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            CharSequence channelname="My Notification";
            int important=NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel= new NotificationChannel(CHANNELID,channelname,important);
            manager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder builder=new NotificationCompat.Builder(context,CHANNELID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("Today Movie Release")
                .setContentText(message)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setWhen(t)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent);

        manager.notify(notificationid,builder.build() );

    }
}
