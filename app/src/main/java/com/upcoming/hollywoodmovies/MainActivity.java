package com.upcoming.hollywoodmovies;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.AddsClass;
import com.AnimationMovieActivity;
import com.ComedyMoviesDetailsActivity;
import com.HorrorMoviesActivity;
import com.MoviesDetailActivity;
import com.MyListAdapter;
import com.MyListData;
import com.RomanceActivity;
import com.WatchListActivity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    RecyclerView recyclerView;
    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView;
    ActionBarDrawerToggle actionBarDrawerToggle;
    RelativeLayout relativeLayout,rv2,rv4,rv3,rv5;
    ImageView back,toolbar_save,action;
    AdView adview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar=findViewById(R.id.tool);
        drawer = findViewById(R.id.drlayout);
        back=toolbar.findViewById(R.id.back);
        action=toolbar.findViewById(R.id.action);
        adview=findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        adview.loadAd(adRequest);
        toolbar_save=toolbar.findViewById(R.id.toolbar_save);
        back.setVisibility(View.GONE);
       // toolbar_save.setVisibility(View.VISIBLE);
        relativeLayout = findViewById(R.id.relativeLayout);
        rv2 = findViewById(R.id.rv2);
        rv4 = findViewById(R.id.rv4);
        rv3 = findViewById(R.id.rv3);
        rv5 = findViewById(R.id.rv5);


        MyListData[] myListData = new MyListData[] {
                new MyListData(R.drawable.banner,"Action"),
                new MyListData(R.drawable.banner,"Comedy"),
                new MyListData(R.drawable.banner,"Romantic"),
                new MyListData(R.drawable.banner,"Horror"),
                new MyListData(R.drawable.banner,"Thriller"),




        };

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv);
        MyListAdapter adapter = new MyListAdapter(myListData,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        navigationView = (NavigationView)findViewById(R.id.nv);
        initNavigationDrawer();




        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, MoviesDetailActivity.class);
                startActivity(intent);


            }
        });
        rv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddsClass addsClass=new AddsClass(MainActivity.this);
                addsClass.showInterstitial();
                Intent intent=new Intent(MainActivity.this, ComedyMoviesDetailsActivity.class);
                startActivity(intent);


            }
        });
        rv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, HorrorMoviesActivity.class);
                startActivity(intent);


            }
        });
        rv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddsClass addsClass=new AddsClass(MainActivity.this);
                addsClass.showInterstitial();
                Intent intent=new Intent(MainActivity.this, RomanceActivity.class);
                startActivity(intent);


            }
        });
        rv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, AnimationMovieActivity.class);
                startActivity(intent);


            }
        });
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                drawer.openDrawer(GravityCompat.START);
//                return true;
//        }

        if (actionBarDrawerToggle.onOptionsItemSelected(item)){

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navmenu, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }


    public void initNavigationDrawer() {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();
//                if (id == R.id.nav_invite) {
//                    // Handle the camera action
//                } else if (id == R.id.track) {

//                    Intent intent=new Intent(MainActivity.this,DateActivity.class);
//                    startActivity(intent);
//                }
                if (id == R.id.share) {

                    try {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Movies Notifier");
                        String shareMessage = "\nHi, Download this cool & fast performance Movies Notifier App.\n";
//                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +"com.appzmachine.notificationmuter"+"\n\n";
                        shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +getPackageName()+"\n\n";
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        startActivity(Intent.createChooser(shareIntent, "choose one"));

                    } catch (Exception e) {

                    }
                } else if (id == R.id.rateus) {

                    try{
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+getPackageName())));
                    }
                    catch (ActivityNotFoundException e){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName())));
                    }
                } else if (id == R.id.privacy) {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1LpNiZyaT105eVeshuL3niy_xejjNN7q2xDZp3zqx2Q4/edit"));
                    startActivity(browserIntent);
                } else if(id==R.id.more){
                    // https://play.google.com/store/apps/developer?id=Appz+machine
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Appz+machine"));
                    startActivity(intent);
                }
                else if(id==R.id.watchlist){
                    // https://play.google.com/store/apps/developer?id=Appz+machine
                    Intent intent = new Intent(MainActivity.this, WatchListActivity.class);
                    startActivity(intent);
                }
                drawer.closeDrawers();


//                switch (id){
//                    case R.id.home:
//                        Toast.makeText(getApplicationContext(),"Home",Toast.LENGTH_SHORT).show();
//                        drawer.closeDrawers();
//                        break;
//
//
//                }
                return true;
            }
        });
        View header = navigationView.getHeaderView(0);

        drawer = findViewById(R.id.drlayout);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close){

            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


}
